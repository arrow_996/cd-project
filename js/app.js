$(document).ready(function(){

$("#list").on('click','#addmembtn',function(event){
  event.preventDefault();
  var Name = $("#name").val();
  var Pass = $("#password").val();
  console.log(Name+" "+Pass)
   $.post(
    'add_record.php',
    {
      action:"insert_member",
      mem_name:Name,
      Password:Pass
    },
    function(res)
    {
      console.log(res);
      swal("Good job!", "Member has been Added successfully", "success")
      $("#name").val("");
      $("#password").val("");
    }
  );
});


$("#list").on('click','#addadminbtn',function(event){
  event.preventDefault();
  var Name = $("#admin_name").val();
  var Pass = $("#admin_password").val();
  console.log(Name+" "+Pass)
   $.post(
    'add_record.php',
    {
      action:"insert_admin",
      admin_name:Name,
      admin_Pass:Pass
    },
    function(res)
    {
      console.log(res);
      swal("Good job!", "Admin has been Added successfully", "success")
      $("#admin_name").val("");
      $("#admin_password").val("");
    }
  );
});
});